import { POMODORO_BREAK_START, POMODORO_STOP, POMODORO_CHECK_TIME, POMODORO_START } from '../constants/actionTypes.jsx'

export const setPomodoroBreakTimer = (subreddit) => {
  return {
    type: POMODORO_BREAK_START,
    subreddit
  }
}

export const checkPomodoroTime = (subreddit) => {
  return {
    type: POMODORO_CHECK_TIME,
    subreddit
  }
}

export const setPomodoroTimer = () => {
  return {
    type: POMODORO_START
  }
}

export const stopPomodoroTimer = () => {
  return {
    type: POMODORO_STOP
  }
}
