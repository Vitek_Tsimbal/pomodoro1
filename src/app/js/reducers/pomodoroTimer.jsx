import { POMODORO_CHECK_TIME,
  POMODORO_START,
  POMODORO_STOP,
  POMODORO_BREAK_START
} from '../constants/actionTypes.jsx'
import objectAssign from 'object-assign'
import { parseTime } from '../helpers/parseTime.js'
import { checkPomodoroTimer } from '../services/checkTimer.js'
import moment from 'moment'

const initialState = {
  currentTime: false,
  timeLeft: undefined,
  isTimerStarted: false,
  isWorkingTime: false,
  breakCount: 0,
  isEnded: false,
  workCount: 0
}

export default function pomodoroTimer (state = initialState, action) {
	// Checking request type
  switch (action.type) {
    case POMODORO_START:
      // Return new object
      return objectAssign({}, state, {
        isTimerStarted: true,
        isWorkingTime: true,
        isEnded: false,
        currentTime: new Date(),
        workCount: state.workCount + 1
      })
    case POMODORO_STOP:
      // Return new object
      return objectAssign({}, state, {
        isTimerStarted: false,
        isWorkingTime: false,
        currentTime: null,
        timeLeft: undefined,
        workCount: 0,
        breakCount: 0
      })
    case POMODORO_BREAK_START:
      return objectAssign({}, state, {
        isTimerStarted: true,
        isWorkingTime: false,
        isEnded: false,
        breakCount: action.subreddit.breakCount !== 4 ? action.subreddit.breakCount + 1 : 0,
        currentTime: new Date()
      })
    case POMODORO_CHECK_TIME:
      // Setting started time
      let startedDateCheck = new Date(Date.parse(state.currentTime))
      // Setting time to end of work
      let checkTime = checkPomodoroTimer(startedDateCheck, state.breakCount, state.isWorkingTime)
      // Check if we have time before work time is ended
      if (moment(checkTime).isAfter(startedDateCheck) === false && state.isWorkingTime === true) {
        return objectAssign({}, state, {
          isTimerStarted: true,
          isWorkingTime: true,
          timeLeft: parseTime(state.currentTime, 'workTime'),
          isEnded: false
        })
      } else if (moment(new Date()).isAfter(checkTime) === false && state.isWorkingTime === false) {
        return objectAssign({}, state, {
          isTimerStarted: true,
          isWorkingTime: false,
          timeLeft: parseTime(checkTime, 'breakTime'),
          isEnded: false
        })
      } else {
        return objectAssign({}, state, {isTimerStarted: false, isEnded: true, timeLeft: null})
      }
    default:
      return state
  }
}