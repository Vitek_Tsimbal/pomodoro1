import { combineReducers } from 'redux'
import pomodoroTimer from './pomodoroTimer.jsx'
import { routerReducer } from 'react-router-redux'

const rootReducer = combineReducers({
  routing: routerReducer,
  pomodoroTimer: pomodoroTimer
})

export default rootReducer