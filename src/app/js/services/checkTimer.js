import { POMODORO_BREAK_TIME_SECONDS, POMODORO_BIG_BREAK_TIME_SECONDS, POMODORO_TIME_SECONDS } from '../constants/appConstants.jsx'

export function checkPomodoroTimer (startedDateCheck, breakCount, isWorkingTime) {
  if (isWorkingTime === true) {
    return (new Date(new Date() - (1 * POMODORO_TIME_SECONDS * 1000)))
  } else {
    if (breakCount !== 4) {
      return (startedDateCheck.getTime() + (1 * POMODORO_BREAK_TIME_SECONDS * 1000))
    } else {
      return (startedDateCheck.getTime() + (1 * POMODORO_BIG_BREAK_TIME_SECONDS * 1000))
    }
  }
}