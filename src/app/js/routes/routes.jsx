import React, { Component } from 'react'
import { Router, Route, browserHistory } from 'react-router'
import { Provider } from 'react-redux'
import configureStore from './../store/configureStore.jsx'
import { syncHistoryWithStore } from 'react-router-redux'
import Pomodoro from './../components/pomodoro.jsx'
const store = configureStore()

class Routes extends Component {
  render () {
    const history = syncHistoryWithStore(browserHistory, store)
    return (
      <div>
        <Provider store={store}>
          <Router history={history}>
            <Route path='/' component={Pomodoro} />
          </Router>
        </Provider>
      </div>
    )
  }
}

export default Routes
