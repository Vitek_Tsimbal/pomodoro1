import rootReducer from '../reducers/root.jsx'
import { createStore, compose, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

const enhancer = compose(
   applyMiddleware(thunk)
)

export default function configureStore (initialState) {
  initialState = initialState || {}
  const store = createStore(rootReducer, initialState, enhancer)
  return store
}