import React, { Component } from 'react'

class StartTimer extends Component {
  render () {
    return (
      <button onClick={this.props.startTimer} className='btn btn-success btn-timer' >Start pomodoro timer</button>
    )
  }
}

StartTimer.PropTypes = {
  startTimer: React.PropTypes.func.isRequired
}


export default StartTimer
