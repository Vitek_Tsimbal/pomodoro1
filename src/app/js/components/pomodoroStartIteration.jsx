import React, { Component } from 'react'
import classNames from 'classnames'

class StartIteration extends Component {
  constructor (props) {
    super(props)

    this.renderButtonText = this.renderButtonText.bind(this)
  }

  renderButtonText () {
    if (!this.props.isWorkingTime) {
      return ('Start work time')
    } else {
      return ('Start break time')
    }
  }

  render () {
    return (
      <button className={
        classNames({'display-none': this.props.isEnded === false || this.props.currentTime === null}) +
        ' btn btn-success btn-timer btn-next-iteration'} onClick={this.props.startTimer}>{this.renderButtonText()}</button>
    )
  }
}

StartIteration.PropTypes = {
  stopPomodoroTimer: React.PropTypes.func.isRequired,
  isEnded: React.PropTypes.bool.isRequired,
  currentTime: React.PropTypes.obj,
  startTimer: React.PropTypes.obj,
  isWorkingTime: React.PropTypes.bool.isRequired
}

export default StartIteration
