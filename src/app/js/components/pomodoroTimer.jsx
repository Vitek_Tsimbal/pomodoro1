import React from 'react'
import classNames from 'classnames'

class Timer extends React.Component {
  constructor (props) {
    super(props)

    this.renderTimer = this.renderTimer.bind(this)

    this.state = {
      hoursStatusAnimation: true,
      minutesStatusAnimation: true,
      secondsStatusAnimation: true
    }
  }

  componentWillReceiveProps (nextProps) {
    if (this.props.timeLeft && nextProps.timeLeft) {
      if (nextProps.timeLeft.hours !== this.props.timeLeft.hours) {
        this.setState({ hoursStatusAnimation: !this.state.hoursStatusAnimation })
      }
      if (nextProps.timeLeft.minutes !== this.props.timeLeft.minutes) {
        this.setState({ minutesStatusAnimation: !this.state.minutesStatusAnimation })
      }
      if (nextProps.timeLeft.seconds !== this.props.timeLeft.seconds) {
        this.setState({ secondsStatusAnimation: !this.state.secondsStatusAnimation })
      }
    }
  }

  renderTimer () {
    if (this.props.timeLeft === undefined || this.props.timeLeft === null) {
      return (
        <h3 className='text-center'>Loading timer...</h3>
      )
    } else {
      return (
        <h3 className={classNames(
          {'pomodoro__working-time': this.props.isWorkingTime === true},
          {'pomodoro__break-time': this.props.isWorkingTime === false}
          ) + ' text-center'}>Time:
          <span className={classNames(
            {'rotate-animation-true' : this.state.hoursStatusAnimation === true},
            {'rotate-animation-false' : this.state.hoursStatusAnimation === false}
            ) + ' pomodoro__time-hours-value'}> {this.props.timeLeft.hours} </span>:
          <span className={classNames(
            {'rotate-animation-true' : this.state.minutesStatusAnimation === true},
            {'rotate-animation-false' : this.state.minutesStatusAnimation === false}
            ) + ' pomodoro__time-minutes-value'}> {this.props.timeLeft.minutes} </span>:
          <span className={classNames(
            {'rotate-animation-true' : this.state.secondsStatusAnimation === true},
            {'rotate-animation-false' : this.state.secondsStatusAnimation === false}
            ) + ' pomodoro__time-seconds-value'}> {this.props.timeLeft.seconds} </span>
        </h3>
      )
    }
  }

  render () {
    return (
      <div className={classNames({'display-none': this.props.isEnded === true})}>
        {this.renderTimer()}
      </div>
    )
  }
}

Timer.PropTypes = {
  timeLeft: React.PropTypes.object.isRequired,
  isEnded: React.PropTypes.bool.isRequired,
  isWorkingTime: React.PropTypes.bool.isRequired
}

export default Timer