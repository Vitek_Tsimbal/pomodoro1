import React, { Component } from 'react'
import { stopPomodoroTimer, checkPomodoroTime, setPomodoroTimer, setPomodoroBreakTimer } from '../actions/pomodoroActions.jsx'
import StartTimer from './pomodoroStart.jsx'
import StopTimer from './pomodoroStop.jsx'
import { connect } from 'react-redux'
import Timer from './pomodoroTimer.jsx'
import StartIteration from './pomodoroStartIteration.jsx'
import AudioSignal from './pomodoroAudioSignal.jsx'

class Pomodoro extends Component {
  constructor (props) {
    super(props)


    this.intervalToCheck = ''
    this.renderTimer = this.renderTimer.bind(this)
    this.onCheckTimer = this.onCheckTimer.bind(this)
    this.startTimer = this.startTimer.bind(this)
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.isTimerStarted === false) {
      window.clearInterval(this.intervalToCheck)

      if (!nextProps.isTimerStarted && nextProps.isEnded && nextProps.isWorkingTime === true && nextProps.workCount !== 0) {
        new Notification('Work time is ended', {
          icon: './images/pomodoro_icon.png'
        })
      } else if (!nextProps.isTimerStarted && nextProps.isEnded && nextProps.isWorkingTime === false && nextProps.breakCount !== 0) {
        new Notification('Break time is ended', {
          icon: './images/pomodoro_icon.png'
        })
      }
    }
  }

  startTimer () {
    if (this.props.isWorkingTime === true) {
      // Start pomodoro break timer
      this.props.startPomodoroBreakTimer({ breakCount: this.props.breakCount })
    } else {
      this.props.setPomodoroTimer()
    }

    this.intervalToCheck = setInterval(this.onCheckTimer, 1000)
  }

  onCheckTimer () {
    if (this.props.isTimerStarted === true && this.props.isEnded === false) {
      this.props.checkPomodorTimeLeft()
    }
  }

  renderTimer () {
    // Check first load
    if (this.props.currentTime === false || !this.props.currentTime) {
      return (
        <div>
          <h3 className='text-center'>Pomodoro timer</h3>
          <StartTimer startTimer={this.startTimer} />
          <StartIteration isEnded={this.props.isEnded} currentTime={this.props.currentTime} startTimer={this.startTimer} />
        </div>
      )
    } else if (this.props.isWorkingTime) {
      return (
        <div>
          <h3 className='text-center'>Work Time</h3>
          <StopTimer stopPomodoroTimer={this.props.stopPomodoroTimer} />
          <h3 className='text-center'>Current work count: {this.props.workCount}</h3>
          <Timer
            timeLeft={this.props.timeLeft}
            isEnded={this.props.isEnded}
            isWorkingTime={this.props.isWorkingTime} />
          <StartIteration
            isEnded={this.props.isEnded}
            currentTime={this.props.currentTime}
            startTimer={this.startTimer}
            isWorkingTime={this.props.isWorkingTime}
            />
        </div>
      )
    } else if (this.props.isWorkingTime === false) {
      return (
        <div>
          <h3 className='text-center'>Break Time</h3>
          <StopTimer stopPomodoroTimer={this.props.stopPomodoroTimer} />
          <h3 className='text-center'>Current break: {this.props.breakCount} / 4 </h3>
          <Timer timeLeft={this.props.timeLeft} isEnded={this.props.isEnded} isWorkingTime={this.props.isWorkingTime} />
          <StartIteration
            isEnded={this.props.isEnded}
            currentTime={this.props.currentTime}
            startTimer={this.startTimer}
            isWorkingTime={this.props.isWorkingTime}
            />
        </div>
      )
    }
  }

  render () {
    return (
      <div>
        <div className='pomodoro__icon-container margin-auto'>
          <img src='./images/pomodoro_icon.png' alt='Pomodoro' className='animated flipInY' />
        </div>
        {this.renderTimer()}
        <AudioSignal isTimerStarted={this.props.isTimerStarted} isEnded={this.props.isEnded} currentTime={this.props.currentTime} />
      </div>
    )
  }
}

Pomodoro.PropTypes = {}

var mapStateToProps = function (state, ownProps) {
  return {
    currentTime: state.pomodoroTimer.currentTime,
    timeLeft: state.pomodoroTimer.timeLeft,
    isTimerStarted: state.pomodoroTimer.isTimerStarted,
    isWorkingTime: state.pomodoroTimer.isWorkingTime,
    isEnded: state.pomodoroTimer.isEnded,
    breakCount: state.pomodoroTimer.breakCount,
    workCount: state.pomodoroTimer.workCount
  }
}

var mapDispatchToProps = (dispatch) => ({
  setPomodoroTimer () {
    dispatch(setPomodoroTimer())
  },
  stopPomodoroTimer () {
    dispatch(stopPomodoroTimer())
  },
  startPomodoroBreakTimer (subreddit) {
    dispatch(setPomodoroBreakTimer(subreddit))
  },
  checkPomodorTimeLeft () {
    dispatch(checkPomodoroTime())
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Pomodoro)