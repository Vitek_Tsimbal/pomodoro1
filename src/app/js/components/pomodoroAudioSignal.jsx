import React, { Component } from 'react'
const pomodoroSignal = './audio/pomodoro-signal.mp3'
import ReactDOM from 'react-dom'
import classNames from 'classnames'

class AudioSignal extends Component {
  constructor (props) {
    super(props)

    this.state = {
      showStopSignalButton: true
    }

    this.renderAudioPlayer = this.renderAudioPlayer.bind(this)
    this.stopAudioSignal = this.stopAudioSignal.bind(this)
  }

  componentWillReceiveProps (nextProps) {
    if (!nextProps.isTimerStarted && nextProps.isEnded) {
      this.state = {
        showStopSignalButton: true
      }
    }
  }

  stopAudioSignal () {
    let nodeSignal = ReactDOM.findDOMNode(this.refs.audioSignal)
    nodeSignal.pause()
    nodeSignal.currentTime = 0

    this.setState({
      showStopSignalButton: false
    })
  }

  renderAudioPlayer () {
    // Check if timer is ended and pomodoro is started
    if (!this.props.isTimerStarted && this.props.isEnded && this.props.currentTime !== null) {
      return (
        <div>
          <div className='hidden-player'>
            <audio controls autoPlay ref='audioSignal'>
              <source src={pomodoroSignal} type='audio/mpeg' autoPlay />
              Тег audio не поддерживается вашим браузером.
            </audio>
          </div>
          <button className={'btn btn-danger btn-timer btn-next-iteration ' + classNames(
            {'display-none': this.state.showStopSignalButton === false})} onClick={this.stopAudioSignal}> Stop Audio signal</button>
        </div>
      )
    } else {
      return null
    }
  }

  render () {
    return (
      <div>
        {this.renderAudioPlayer()}
      </div>
    )
  }
}

AudioSignal.PropTypes = {
  isTimerStarted: React.PropTypes.bool.isRequired,
  isEnded: React.PropTypes.bool.isRequired,
  currentTime: React.PropTypes.obj
}


export default AudioSignal
