import React, { Component } from 'react'

class StopTimer extends Component {
  render () {
    return (
      <button onClick={this.props.stopPomodoroTimer} className='btn btn-success btn-timer'>Stop pomodoro timer</button>
    )
  }
}

StopTimer.PropTypes = {
  stopPomodoroTimer: React.PropTypes.func
}

export default StopTimer
