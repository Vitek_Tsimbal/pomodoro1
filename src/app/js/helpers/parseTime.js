export function parseTime (timeLeft, timeIs) {
  let t = Date.parse(new Date()) - Date.parse(new Date(timeLeft))
  if (timeIs === 'workTime') {
    t = Date.parse(new Date()) - Date.parse(new Date(timeLeft))
  } else {
    t = Date.parse(new Date(timeLeft)) - Date.parse(new Date())
  }

  let seconds = Math.floor((t / 1000) % 60)
  let minutes = Math.floor((t / 1000 / 60) % 60)
  let hours = Math.floor((t / (1000 * 60 * 60)) % 24)
  let days = Math.floor(t / (1000 * 60 * 60 * 24))

  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  }
}