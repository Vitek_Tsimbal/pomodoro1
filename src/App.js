import React, { Component } from 'react'
// import './App.css'
import Routes from './app/js/routes/routes.jsx'
import css from './app/styles/importer.less'

class App extends Component {
  componentDidMount () {
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.register('/sw.js')
      if (Notification.requestPermission) {
        Notification.requestPermission(function (result) {
          console.log('Notificationission : ', result)
        })
      } else {
        console.log('Notifications supported by this browser.')
      }
    }
  }

  render () {
    return (
      <div className='container'>
        <Routes />
      </div>
    )
  }
}

export default App
