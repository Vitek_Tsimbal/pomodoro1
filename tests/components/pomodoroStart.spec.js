import StartTimer from '../../src/app/js/components/pomodoroStart.jsx'
import React from 'react'
import { mount, shallow, render } from 'enzyme'
import {expect} from 'chai'
import { createStore, combineReducers } from 'redux'
import { spy } from 'sinon'

describe('<StartTimer>', function () {
	it('should have a button for display the content', function () {
		const wrapper = render(<StartTimer />)
		expect(wrapper.find('button')).to.have.length.not(0)
	})
  it('should props to be defined', function () {
    const wrapper = shallow(<StartTimer />)
    expect(wrapper.props().startTimer).to.be.defined
  })
	it('should not have any onloads', function () {
    const dispatchSpy = spy()
    const wrapper = shallow(<StartTimer dispatch={dispatchSpy} />)
    expect(dispatchSpy.callCount).to.equal(0)
  })
})