import Timer from '../../src/app/js/components/pomodoroTimer.jsx'
import React from 'react'
import { mount, shallow, render } from 'enzyme'
import {expect} from 'chai'
import { spy } from 'sinon'

describe('<Timer>', function () {
	it('should have a div for display the content', function () {
		const wrapper = render(<Timer />)
		expect(wrapper.find('div')).to.have.length(1)
	})
  it('should props to be defined', function () {
    const wrapper = shallow(<Timer />)
    expect(wrapper.props().timeLeft).to.be.defined
    expect(wrapper.props().isEnded).to.be.defined
    expect(wrapper.props().isWorkingTime).to.be.defined
  })
	it('should not have any onloads', function () {
    const dispatchSpy = spy()
    const wrapper = shallow(<Timer dispatch={dispatchSpy} />)
    expect(dispatchSpy.callCount).to.equal(0)
  })
  it('should have a div for display the content', function () {
		const wrapper = render(<Timer />)
		expect(wrapper.find('h3')).to.have.length(1)
	})
})