import AudioSignal from '../../src/app/js/components/pomodoroAudioSignal.jsx'
import React from 'react'
import { mount, shallow, render } from 'enzyme'
import {expect} from 'chai'
import { createStore, combineReducers } from 'redux'
import { spy } from 'sinon'

describe('<AudioSignal>', function () {
  it('should have a div for display the content', function () {
    const dispatchSpy = spy()
    const rootReducer = combineReducers({
      pomodoroTimer: function () {
        const pomodoroTimer = {}
        return pomodoroTimer
      }
    })
    const store = createStore(rootReducer)
    const wrapper = render(<AudioSignal store={store} dispatch={dispatchSpy} />)
    expect(wrapper.find('div')).to.have.length.not(0)
  })
  it('should have a audio for display the content', function () {
    const dispatchSpy = spy()
    const rootReducer = combineReducers({
      pomodoroTimer: function () {
        const pomodoroTimer = {}
        return pomodoroTimer
      }
    })
    const store = createStore(rootReducer)
    const wrapper = render(<AudioSignal isTimerStarted={false} isEnded={true} currentTime={1} />)
    expect(wrapper.find('audio')).to.have.length.not(0)
  })
  it('should have a source for display the content', function () {
    const dispatchSpy = spy()
    const rootReducer = combineReducers({
      pomodoroTimer: function () {
        const pomodoroTimer = {}
        return pomodoroTimer
      }
    })
    const store = createStore(rootReducer)
    const wrapper = render(<AudioSignal isTimerStarted={false} isEnded={true} currentTime={1} />)
    expect(wrapper.find('source')).to.have.length.not(0)
  })
  it('should have a source for display the content', function () {
    const dispatchSpy = spy()
    const rootReducer = combineReducers({
      pomodoroTimer: function () {
        const pomodoroTimer = {}
        return pomodoroTimer
      }
    })
    const store = createStore(rootReducer)
    const wrapper = render(<AudioSignal isTimerStarted={false} isEnded={true} currentTime={1} />)
    expect(wrapper.find('button')).to.have.length.not(0)
  })
  it('should not have any onloads', function () {
    const dispatchSpy = spy()
    const rootReducer = combineReducers({
      pomodoroTimer: function () {
        const pomodoroTimer = {
          currentTime: 123
        }
        return pomodoroTimer
      }
    })
    const store = createStore(rootReducer)
    const wrapper = shallow(<AudioSignal store={store} dispatch={dispatchSpy} />)
    expect(dispatchSpy.callCount).to.equal(0)
  })
  it('should props to be defined', function () {
    const wrapper = shallow(<AudioSignal />)
    expect(wrapper.props().isTimerStarted).to.be.defined
    expect(wrapper.props().isEnded).to.be.defined
    expect(wrapper.props().currentTime).to.be.defined
  })
})