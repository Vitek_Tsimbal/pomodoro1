import Pomodoro from '../../src/app/js/components/pomodoro.jsx'
import React from 'react'
import { mount, shallow, render } from 'enzyme'
import {expect} from 'chai'
import { createStore, combineReducers } from 'redux'
import { spy } from 'sinon'

describe('<Pomodoro>', function () {
  it('should have a div for display the content', function () {
    const dispatchSpy = spy()
    const rootReducer = combineReducers({
      pomodoroTimer: function () {
        const pomodoroTimer = {}
        return pomodoroTimer
      }
    })
    const store = createStore(rootReducer)
    const wrapper = render(<Pomodoro store={store} dispatch={dispatchSpy} />)
    expect(wrapper.find('div')).to.have.length.not(0)
  })
  it('should have a renderTimer for display the content', function () {
    const dispatchSpy = spy()
    const rootReducer = combineReducers({
      pomodoroTimer: function () {
        const pomodoroTimer = {}
        return pomodoroTimer
      }
    })
    const store = createStore(rootReducer)
    const wrapper = render(<Pomodoro store={store} dispatch={dispatchSpy} />)
    expect(wrapper.find('renderTimer')).to.have.length(0)
  })
  it('should have a AudioSignal for display the content', function () {
    const dispatchSpy = spy()
    const rootReducer = combineReducers({
      pomodoroTimer: function () {
        const pomodoroTimer = {}
        return pomodoroTimer
      }
    })
    const store = createStore(rootReducer)
    const wrapper = render(<Pomodoro store={store} dispatch={dispatchSpy} />)
    expect(wrapper.find('AudioSignal')).to.have.length(0)
  })
  it('should not have any onloads', function () {
    const dispatchSpy = spy()
    const rootReducer = combineReducers({
      pomodoroTimer: function () {
        const pomodoroTimer = {
          currentTime: 123
        }
        return pomodoroTimer
      }
    })
    const store = createStore(rootReducer)
    const wrapper = shallow(<Pomodoro store={store} dispatch={dispatchSpy} />)
    expect(dispatchSpy.callCount).to.equal(0)
  })
})