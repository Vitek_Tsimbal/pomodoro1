import StartIteration from '../../src/app/js/components/pomodoroStartIteration.jsx'
import React from 'react'
import { mount, shallow, render } from 'enzyme'
import {expect} from 'chai'
import { createStore, combineReducers } from 'redux'
import { spy } from 'sinon'

describe('<StartIteration>', function () {
  it('should have a div for display the content', function () {
    const dispatchSpy = spy()
    const rootReducer = combineReducers({
      pomodoroTimer: function () {
        const pomodoroTimer = {}
        return pomodoroTimer
      }
    })
    const store = createStore(rootReducer)
    const wrapper = render(<StartIteration store={store} dispatch={dispatchSpy} />)
    expect(wrapper.find('button')).to.have.length.not(0)
  })
  it('should not have any onloads', function () {
    const dispatchSpy = spy()
    const rootReducer = combineReducers({
      pomodoroTimer: function () {
        const pomodoroTimer = {
          currentTime: 123
        }
        return pomodoroTimer
      }
    })
    const store = createStore(rootReducer)
    const wrapper = shallow(<StartIteration store={store} dispatch={dispatchSpy} />)
    expect(dispatchSpy.callCount).to.equal(0)
  })
  it('should props to be defined', function () {
    const wrapper = shallow(<StartIteration />)
    expect(wrapper.props().stopPomodoroTimer).to.be.defined
    expect(wrapper.props().isEnded).to.be.defined
    expect(wrapper.props().currentTime).to.be.defined
    expect(wrapper.props().startTimer).to.be.defined
    expect(wrapper.props().isWorkingTime).to.be.defined
  })
})