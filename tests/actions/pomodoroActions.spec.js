import {expect} from 'chai'
import * as actions from '../../src/app/js/actions/pomodoroActions.jsx'
import * as types from '../../src/app/js/constants/ActionTypes.jsx'
// Async Action Creators import
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import nock from 'nock'

const middlewares = [ thunk ]
const mockStore = configureMockStore(middlewares)

describe('Pomodoro actions', () => {
  it('should create an action POMODORO_BREAK_START', () => {
    const subreddit = {
      type: 'POMODORO_BREAK_START',
      currentTime: '123'
    }
    const expectedAction = {
      type: types.POMODORO_BREAK_START,
      currentTime: '123'
    }
    expect(subreddit).to.eql(expectedAction)
  })
  it('should create an action POMODORO_CHECK_TIME', () => {
    const subreddit = {
      type: 'POMODORO_CHECK_TIME',
      currentTime: '123'
    }
    const expectedAction = {
      type: types.POMODORO_CHECK_TIME,
      currentTime: '123'
    }
    expect(subreddit).to.eql(expectedAction)
  })
  it('should create an action POMODORO_START', () => {
    const subreddit = {
      type: 'POMODORO_START'
    }
    const expectedAction = {
      type: types.POMODORO_START
    }
    expect(subreddit).to.eql(expectedAction)
  })
  it('should create an action POMODORO_START', () => {
    const subreddit = {
      type: 'POMODORO_STOP'
    }
    const expectedAction = {
      type: types.POMODORO_STOP
    }
    expect(subreddit).to.eql(expectedAction)
  })
})