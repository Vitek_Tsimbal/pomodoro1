import reducer from '../../src/app/js/reducers/pomodoroTimer.jsx'
import {expect} from 'chai'
import * as types from '../../src/app/js/constants/ActionTypes.jsx'
import deepFreeze from 'deep-freeze'
/* global describe, it */


describe('Pomodoro reducer', () => {
  it('should return the initial state', () => {
    expect(
      reducer({
        currentTime: null,
        timeLeft: undefined,
        isTimerStarted: false,
        isWorkingTime: false,
        breakCount: 0,
        workCount: 0
      }, {})
    ).to.eql({
      currentTime: null,
      timeLeft: undefined,
      isTimerStarted: false,
      isWorkingTime: false,
      breakCount: 0,
      workCount: 0
    })
  })
  it('should handle POMODORO_START', () => {
    const before = {
      isTimerStarted: true,
      isWorkingTime: true,
      isEnded: false,
      workCount: 1,
      currentTime: new Date()
    }
    deepFreeze(before)
    expect(
      reducer(before, {
        type: types.POMODORO_START
      })
    ).to.eql({
      isTimerStarted: true,
      isWorkingTime: true,
      isEnded: false,
      workCount: 2,
      currentTime: new Date()
    })
  })
  it('should handle POMODORO_STOP', () => {
    const before = {
      isTimerStarted: false,
      isWorkingTime: false,
      currentTime: null,
      timeLeft: undefined,
      breakCount: 0,
      workCount: 0
    }
    deepFreeze(before)
    expect(
      reducer(before, {
        type: types.POMODORO_STOP
      })
    ).to.eql(before)
  })
  it('should handle POMODORO_BREAK_START', () => {
    const breakCount = 0
    const before = {
      isTimerStarted: true,
      isWorkingTime: false,
      isEnded: false,
      breakCount: breakCount + 1,
      currentTime: new Date()
    }
    deepFreeze(before)
    expect(
      reducer(before, {
        type: types.POMODORO_BREAK_START,
        subreddit: {
          breakCount: breakCount
        }
      })
    ).to.eql(before)
  })
  it('should handle POMODORO_CHECK_TIME', () => {
    const before = {
      subreddit: {},
      timeLeft: null,
      isTimerStarted: false,
      isEnded: true
    }
    deepFreeze(before)
    expect(
      reducer(before, {
        type: types.POMODORO_CHECK_TIME
      })
    ).to.eql(before)
  })
})