require('babel-register')
require('babel-core')
require('babel-preset-es2015')
require('babel-preset-react')
require('babel-preset-stage-0')

var jsdom = require('jsdom').jsdom

var exposedProperties = ['window', 'navigator', 'document']

global.document = jsdom('<!doctype html><html><body></body></html>')
global.window = document.defaultView

// Fix for: Error: matchMedia not present, legacy browsers require a polyfill
window.matchMedia = window.matchMedia || function () {
  return {
    matches: false,
    addListener: function () {},
    removeListener: function () {}
  }
}

Object.keys(document.defaultView).forEach((property) => {
  if (typeof global[property] === 'undefined') {
    exposedProperties.push(property)
    global[property] = document.defaultView[property]
  }
})

global.navigator = {
  userAgent: 'node.js'
}

documentRef = document