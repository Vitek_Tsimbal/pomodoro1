### **Pomodoro App** ###

## Installation: ##

1) Download application

2) Install Yarn globally

- brew install yarn

3) Make installation with Yarn

- yarn install

4) Build the app

-  yarn build

## Starting app: ##

1) Write in command line:

- yarn start