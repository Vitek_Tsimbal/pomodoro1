self.addEventListener('install', function (event) {
	console.log('Install');
	console.log(event)
	event.waitUntil(
		caches.open('pomodoroApp').then(function (cache) {
      return cache.addAll([
        'https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.29/browser.js',
        '/index.html',
        '/pomodoro',
        '/styles.css',
        '/static/js/bundle.js',
        '/audio/pomodoro-signal.mp3',
        '../src/styles/fonts/DS-DIGIT.TTF',
        '/images/pomodoro_icon.png',
        'https://fonts.googleapis.com/css?family=Plaster',
        '/'
      ])
    })
	)
});

self.addEventListener('fetch', function onServiceWorkerFetch (event) {
  console.log('fetch event', event)
  // Calling event.respondWith means we're in charge
  // of providing the response. We pass in a promise
  // that resolves with a response object
  event.respondWith(
    // First we look if we can get the (maybe updated)
    // resource from the network
    fetch(event.request).then(function updateCacheAndReturnNetworkResponse (networkResponse) {
      console.log('fetch from network for ' + event.request.url + ' successfull, updating cache')
      // caches.open(currentCache).then(function addToCache (cache) {
      //   return cache.add(event.request)
      // })
      return networkResponse
    }).catch(function lookupCachedResponse (reason) {
      // On failure, look up in the Cache for the requested resource
      console.log('fetch from network for ' + event.request.url + ' failed:', reason)
      return caches.match(event.request).then(function returnCachedResponse (cachedResponse) {
        return cachedResponse
      })
    })
  )
});